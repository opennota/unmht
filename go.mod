module gitlab.com/opennota/unmht

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/andybalholm/cascadia v1.1.0
	github.com/pkg/browser v0.0.0-20180916011732-0a3d74bf9ce4
	golang.org/x/net v0.0.0-20191119073136-fc4aabc6c914
	golang.org/x/text v0.3.2 // indirect
)

go 1.13
