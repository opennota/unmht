unmht [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Pipeline status](https://gitlab.com/opennota/unmht/badges/master/pipeline.svg)](https://gitlab.com/opennota/unmht/commits/master)
=====

Currently (08-May-2018), the
[UnMHT](https://addons.mozilla.org/en-US/firefox/addon/unmht/) add-on doesn't
work with Firefox Quantum; nor there is another add-on which supports opening
existing MHT files.  So here is a command-line tool which allows one to view
previously saved MHT web archives in a browser.

## Install

    go install gitlab.com/opennota/unmht@latest

## Use

    unmht previously-saved.mht

## Donate

**Bitcoin (BTC):** `1PEaahXKwJvNJGJa2PXtPFLNYYigmdLXct`

**Ethereum (ETH):** `0x83e9607E693467Cb344244Df10f66c036eC3Dc53`
